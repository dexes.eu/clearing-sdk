# Dexes / Clearing SDK

[gitlab.com/dexes.eu/clearing-sdk](https://gitlab.com/dexes.eu/clearing-sdk)

Dexes package providing a SDK for interacting with the [Clearing component](https://clearing.dev.dexes.eu).

## License

View the `LICENSE.md` file for licensing details.

## Installation

Installation of ...

```shell
composer require dexes/catalog-sdk
```

# Usage

### Laravel

Add the following code to the AppServiceProvider.

```PHP
$this->app->bind(HttpRequestService::class, function() {
    return new HttpRequestService(
        'https://policystore.dev.dexes.eu',
        $this->app->make(ClientInterface::class),
        $this->app->make(RequestFactoryInterface::class),
        $this->app->make(StreamFactoryInterface::class),
    );
});

$this->app->bind(AuthRepository::class, function() {
    return new AuthRepository(
        $this->app->make(HttpRequestService::class),
        config('services.ishare.satellites.DMI.x5c'),
        'NL.EORI.LIFEELEC4DMI',
        config('services.ishare.clientId'),
        config('services.ishare.satellites.DMI.private-key')
    );
});

$this->app->bind(ClearingSdk::class, function() {
    return new ClearingSdk(
        $this->app->make(HttpRequestService::class),
        $this->app->make(AuthRepository::class),
        $this->app->make(LoggerInterface::class),
        $this->app->make(CacheInterface::class),
    );
});
```

### Drupal

We will create a Drupal Module which will be available [here](https://gitlab.com/dexes.eu/drupal) as soon as it is published
