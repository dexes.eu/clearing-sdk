<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk;

use XpertSelect\PsrTools\PsrResponse;

class ClearingResponse extends PsrResponse
{
    /**
     * {@inheritdoc}
     */
    public function getJsonSchemaPath(): string
    {
        return __DIR__ . '/../resources/json-schemas/';
    }
}
