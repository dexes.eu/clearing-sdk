<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk;

use Dexes\ClearingSdk\Repositories\Auth\AuthRepository;
use Dexes\ClearingSdk\Repositories\ClearingService\ClearingRepository;
use Dexes\ClearingSdk\Repositories\PolicyStore\Dataspace\OfferPolicyRepository;
use Dexes\ClearingSdk\Repositories\PolicyStore\Dataspace\PolicyTemplateRepository;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

class ClearingSdk
{
    private ?PolicyTemplateRepository $policyTemplateRepository;

    private ?OfferPolicyRepository $offerPolicyRepository;

    private ?ClearingRepository $clearingRepository;

    /**
     * ClearingSdk Constructor.
     *
     * @param HttpRequestService $httpRequestService the HttpRequestService object for making HTTP requests
     * @param AuthRepository     $authRepository     the AuthRepository object for handling authentication
     * @param LoggerInterface    $logger             the LoggerInterface object for logging messages
     * @param CacheInterface     $cache              the CacheInterface object for caching data
     */
    public function __construct(protected HttpRequestService $httpRequestService,
                                protected AuthRepository $authRepository,
                                protected LoggerInterface $logger,
                                protected CacheInterface $cache)
    {
        $this->policyTemplateRepository = null;
        $this->offerPolicyRepository    = null;
        $this->clearingRepository       = null;
    }

    /**
     * Returns the PolicyTemplateRepository instance.
     *
     * If the PolicyTemplateRepository instance has not been created yet, it creates a new instance using
     * the makePolicyTemplateRepository() method and assigns it to the $policyTemplateRepository property.
     *
     * @return PolicyTemplateRepository the PolicyTemplateRepository instance
     */
    public function dataspacePolicyTemplate(): PolicyTemplateRepository
    {
        if (is_null($this->policyTemplateRepository)) {
            $this->policyTemplateRepository = $this->makePolicyTemplateRepository();
        }

        return $this->policyTemplateRepository;
    }

    /**
     * Returns the offer policy repository.
     *
     * If the OfferPolicyRepository instance has not been created yet, it creates a new instance using
     * the makePolicyTemplateRepository() method and assigns it to the $offerPolicyRepository property.
     *
     * @return OfferPolicyRepository the offer policy repository
     */
    public function dataspaceOfferPolicy(): OfferPolicyRepository
    {
        if (is_null($this->offerPolicyRepository)) {
            $this->offerPolicyRepository = $this->makeOfferPolicyRepository();
        }

        return $this->offerPolicyRepository;
    }

    /**
     * Return the clearing repository.
     *
     * If the ClearingRepository instance has not been created yet, it creates a new instance using
     * the makeClearingRepository() method and assigns it to the $clearingRepository property.
     *
     * @return ClearingRepository The clearing repository
     */
    public function clearing(): ClearingRepository
    {
        if (is_null($this->clearingRepository)) {
            $this->clearingRepository = $this->makeClearingRepository();
        }

        return $this->clearingRepository;
    }

    /**
     * Creates a new instance of the PolicyTemplateRepository.
     *
     * @return PolicyTemplateRepository the newly created PolicyTemplateRepository instance
     */
    public function makePolicyTemplateRepository(): PolicyTemplateRepository
    {
        return new PolicyTemplateRepository($this->httpRequestService, $this->authRepository, $this->logger, $this->cache);
    }

    /**
     * Create a new instance of OfferPolicyRepository.
     *
     * @return OfferPolicyRepository the newly created OfferPolicyRepository instance
     */
    public function makeOfferPolicyRepository(): OfferPolicyRepository
    {
        return new OfferPolicyRepository($this->httpRequestService, $this->authRepository, $this->logger, $this->cache);
    }

    /**
     * Creates a new instance of ClearingRepository.
     *
     * @return ClearingRepository The newly created ClearingRepository instance
     */
    public function makeClearingRepository(): ClearingRepository
    {
        return new ClearingRepository($this->httpRequestService, $this->authRepository, $this->logger, $this->cache);
    }
}
