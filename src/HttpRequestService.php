<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use SensitiveParameter;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\PsrResponse;

class HttpRequestService
{
    /**
     * The API endpoint to communicate with.
     */
    private string $endpoint;

    /**
     * The API key for authentication and authorization.
     */
    private ?string $apiKey;

    /**
     * HttpRequestService constructor.
     *
     * @param string                  $endpoint       The API endpoint to communicate with
     * @param ClientInterface         $httpClient     The HTTP client to send HTTP requests with
     * @param RequestFactoryInterface $requestFactory The factory for creating HTTP requests
     * @param StreamFactoryInterface  $streamFactory  The factory for creating HTTP streams
     * @param string                  $authHeaderKey  The header key for authorization
     */
    public function __construct(string $endpoint, private readonly ClientInterface $httpClient,
                                private readonly RequestFactoryInterface $requestFactory,
                                private readonly StreamFactoryInterface $streamFactory,
                                private string $authHeaderKey = 'Authorization')
    {
        $this->endpoint = $this->validateEndpoint($endpoint);
        $this->apiKey   = null;
    }

    /**
     * Set the Authorization header key.
     *
     * @param string $name The name of the authorization header
     */
    public function setAuthHeaderKey(string $name): void
    {
        $this->authHeaderKey = $name;
    }

    /**
     * Sets the API key for authentication and authorization.
     *
     * @param null|string $apiKey The API key to send for authentication and authorization
     */
    final public function setApiKey(#[SensitiveParameter] ?string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Returns a boolean that indicates whether the request service has an API key set.
     *
     * @return bool The boolean that indicates whether the request service has an API key set
     */
    final public function hasApiKey(): bool
    {
        return !is_null($this->apiKey);
    }

    /**
     * Retrieve the API endpoint.
     *
     * @return string The API endpoint
     */
    final public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * Send a GET request to the given path with optional query parameters.
     *
     * @param string               $path       The path to send the GET request to
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function get(string $path = '', array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('GET', $path, parameters: $parameters));
    }

    /**
     * Send a POST request to the given path with optional query parameters and optional JSON data.
     *
     * @param string               $path       The path to send the POST request to
     * @param array<string, mixed> $jsonData   The JSON data to send in the body of the POST request
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function post(string $path = '', array $jsonData = [], array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('POST', $path, $jsonData, $parameters));
    }

    /**
     * Send a PUT request to the given path with optional query parameters and optional JSON data.
     *
     * @param string               $path       The path to send the PUT request to
     * @param array<string, mixed> $parameters The set of query parameters
     * @param array<string, mixed> $jsonData   The JSON data to send in the body of the PUT request
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function put(string $path = '', array $parameters = [], array $jsonData = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('PUT', $path, $parameters, $jsonData));
    }

    /**
     * Send a PATCH request to the given path with optional query parameters and optional JSON data.
     *
     * @param string               $path       The path to send the PATCH request to
     * @param array<string, mixed> $parameters The set of query parameters
     * @param array<string, mixed> $jsonData   The JSON data to send in the body of the PATCH request
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function patch(string $path = '', array $parameters = [], array $jsonData = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('PATCH', $path, $parameters, $jsonData));
    }

    /**
     * Send a DELETE request to the given path with optional query parameters.
     *
     * @param string               $path       The path to send the PATCH request to
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    final public function delete(string $path = '', array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createJsonRequest('DELETE', $path, parameters: $parameters));
    }

    /**
     * Sends an x-ww-form-urlencoded request, can be used for m2m authentication.
     *
     * @param string               $path       The path to send the PATCH request to
     * @param array<string, mixed> $jsonData   The JSON data to send in the body of the request
     * @param array<string, mixed> $parameters The set of query parameters
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    public function postEncoded(string $path = '', array $jsonData = [], array $parameters = []): PsrResponse
    {
        return $this->sendRequest($this->createEncodedRequest('POST', $path, $jsonData, $parameters));
    }

    /**
     * Creates a user agent string for the SDK.
     *
     * @return string the user agent string
     */
    public function createUserAgent(): string
    {
        return 'dexes/clearing-sdk';
    }

    /**
     * Create a PsrResponse instance from a given ResponseInterface.
     *
     * @param ResponseInterface $response The response object received from the API
     *
     * @return PsrResponse A PsrResponse object representing the API response
     */
    public function getPsrResponse(ResponseInterface $response): PsrResponse
    {
        return new ClearingResponse($response);
    }

    /**
     * Create an encoded request with the given method, path, and data.
     *
     * @param string               $method     The HTTP method of the request (e.g., "GET", "POST", etc.)
     * @param string               $path       The path to the API endpoint
     * @param array<string, mixed> $jsonData   The JSON data to be included in the request body (default: [])
     * @param array<string, mixed> $parameters The query parameters to be included in the request URL (default: [])
     *
     * @return RequestInterface The encoded HTTP request
     *
     * @throws ClientException Thrown when encoding the JSON data failed
     */
    private function createEncodedRequest(string $method, string $path, array $jsonData = [], array $parameters = []): RequestInterface
    {
        $uri     = $this->createUri($path, $parameters);
        $request = $this->requestFactory->createRequest($method, $uri)
            ->withHeader('User-Agent', $this->createUserAgent());

        if ('POST' === $method) {
            $request = $request->withHeader('Content-Type', 'application/x-www-form-urlencoded');
        }

        if (!empty($jsonData)) {
            $jsonString = json_encode($jsonData);

            if (false === $jsonString) {
                throw new ClientException('Failed to encode json data');
            }
            $request = $request->withBody($this->streamFactory->createStream(http_build_query($jsonData, '', '&')));
        }

        return $request;
    }

    /**
     * Send a given request to the API.
     *
     * @throws ClientException Thrown when the request could not be sent
     */
    private function sendRequest(RequestInterface $request): PsrResponse
    {
        try {
            return $this->getPsrResponse($this->httpClient->sendRequest($request));
        } catch (ClientExceptionInterface $e) {
            throw new ClientException('Failed to send HTTP request to the API', previous: $e);
        }
    }

    /**
     * Ensure the endpoint does not have a trailing '/'.
     *
     * @param string $endpoint The endpoint to validate
     *
     * @return string The validated endpoint
     */
    private function validateEndpoint(string $endpoint): string
    {
        return !empty($endpoint) && str_ends_with($endpoint, '/')
            ? substr($endpoint, 0, -1)
            : $endpoint;
    }

    /**
     * Creates a JSON HTTP request.
     *
     * @param string               $method     The HTTP method
     * @param string               $path       The path to send the request to
     * @param array<string, mixed> $jsonData   The JSON data to send in the body of the request
     * @param array<string, mixed> $parameters The query parameters to include
     *
     * @return RequestInterface The created request
     *
     * @throws ClientException Thrown when the request could not be created
     */
    private function createJsonRequest(string $method, string $path, array $jsonData = [], array $parameters = []): RequestInterface
    {
        $uri     = $this->createUri($path, $parameters);
        $request = $this->requestFactory->createRequest($method, $uri)
            ->withHeader('Accept', 'application/json')
            ->withHeader('User-Agent', $this->createUserAgent());

        if (!empty($this->apiKey)) {
            $request = $request->withHeader($this->authHeaderKey, $this->apiKey);
        }

        if (!empty($jsonData)) {
            $jsonString = json_encode($jsonData);

            if (false === $jsonString) {
                throw new ClientException('Failed to encode json data');
            }

            $request = $request
                ->withHeader('Content-type', 'application/json')
                ->withBody($this->streamFactory->createStream($jsonString));
        }

        return $request;
    }

    /**
     * Creates a URI.
     *
     * @param string               $path       The path of the URI
     * @param array<string, mixed> $parameters The query parameters of the URI
     *
     * @return string The created URI
     */
    private function createUri(string $path, array $parameters = []): string
    {
        $url = $this->endpoint . '/' . $path;

        if (count($parameters) > 0) {
            $url = $url . '?' . http_build_query(array_map(function($element) {
                    if (is_bool($element)) {
                        $element = $element ? 'true' : 'false';
                    }

                    return $element;
                }, $parameters));
        }

        return $url;
    }
}
