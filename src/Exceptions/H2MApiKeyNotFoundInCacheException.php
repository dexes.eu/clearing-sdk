<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk\Exceptions;

use Exception;

/**
 * Class H2MApiKeyNotFoundInCache.
 *
 * Should be thrown when there is no H2M API key in the cache.
 *
 * Requesting such an API key can be done by calling humanToMachineToken function in the
 * AuthRepository.
 */
class H2MApiKeyNotFoundInCacheException extends Exception
{
}
