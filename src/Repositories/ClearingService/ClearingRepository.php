<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk\Repositories\ClearingService;

use Dexes\ClearingSdk\Repositories\BaseRepository;
use Psr\SimpleCache\InvalidArgumentException;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

class ClearingRepository extends BaseRepository
{
    /**
     * Initiates the clearing process for a given offer policy. The calling party (requesterUser)
     * must have the Clearing.Create authorization from the requester party.
     *
     * @param string                    $offerPolicyId    the ID of the offer policy
     * @param string                    $requesterUser    the user making the request
     * @param string                    $requesterCompany the company making the request
     * @param null|array<string, mixed> $webhook          (Optional) The webhook to send notifications to
     *
     * @return array<string, mixed> the response JSON as an array
     *
     * @throws ClientException          if there is an error with the client
     * @throws InvalidArgumentException if there is an error with the cache
     * @throws ResponseException        Is thrown when the request did not succeed
     */
    public function start(string $offerPolicyId, string $requesterUser, string $requesterCompany, ?array $webhook = null): array
    {
        $this->setApiKey();

        $body = [
            'offer_policy_id'   => $offerPolicyId,
            'requester_user'    => $requesterUser,
            'requester_company' => $requesterCompany,
        ];

        if (!is_null($webhook)) {
            $body['webhook'] = $webhook;
        }

        $response = $this->requestService->post('/clearing/start', $body);

        if ($response->hasStatus(401)) {
            $message = 'Unauthorized for clearing request.';

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        if ($response->hasStatus(404)) {
            $message = sprintf('Offer policy (%s) not found', $offerPolicyId);

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        if (!$response->hasStatus(200)) {
            $this->logger->error('Something went wrong while trying to initiate the clearing process');

            throw new ResponseException($response);
        }

        return $response->json(true);
    }

    /**
     * Retrieves a clearing request by its id. The calling party must have the Clearing.Read
     * authorization from the Entitled Party of the Offer Policy or the requester party.
     *
     * @param string $id the id of the clearing to retrieve
     *
     * @return array<string, mixed> the clearing data as an associative array
     *
     * @throws InvalidArgumentException if the cache implementation encounters an error
     * @throws ClientException          if there is an error communicating with the server
     * @throws ResponseException        Is thrown when the request did not succeed
     */
    public function get(string $id): array
    {
        $this->setApiKey();

        $response = $this->requestService->get('/clearing/' . $id);

        if ($response->hasStatus(401)) {
            $message = 'Unauthorized for clearing request.';

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        if ($response->hasStatus(404)) {
            $this->logger->error(sprintf('Clearing request with id - %s - not found.', $id));

            throw new ResponseException($response);
        }

        if (!$response->hasStatus(200)) {
            $this->logger->error('Something went wrong while trying to retrive the clearing status. clearing_id: ' . $id);

            throw new ResponseException($response);
        }

        return $response->json(true);
    }

    /**
     * Retrieves all clearing requests by the requester company. The calling party must have the Clearing.Read
     * authorization from the requester Party.
     *
     * @param string      $requesterCompany the name of the requester company
     * @param null|string $status           the status of the clearing request or null to retrieve all
     *
     * @return array<string, mixed> the clearing data
     *
     * @throws ClientException          if there is an error communicating with the server
     * @throws InvalidArgumentException if the cache implementation encounters an error
     * @throws ResponseException        Is thrown when the request did not succeed
     */
    public function requester(string $requesterCompany, ?string $status = null): array
    {
        $this->setApiKey();

        $url = '/clearing/requester?requester_company=' . $requesterCompany;
        if (!is_null($status)) {
            $url .= '&status=' . $status;
        }

        $response = $this->requestService->get($url);

        if ($response->hasStatus(401)) {
            $message = 'Unauthorized for clearing request.';

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        if (!$response->hasStatus(200)) {
            $this->logger->error('Something went wrong while trying to retrieve the clearing status from request. requester_company: ' . $requesterCompany);

            throw new ResponseException($response);
        }

        return $response->json(true);
    }

    /**
     * This Endpoint returns all clearing requests or one clearing request by id for the Entitled Party.
     * The calling party must have the Clearing.Read authorization from the Entitled Party of the Offer Policy.
     *
     * @param string      $entitledParty the Entitled Party of the clearing request
     * @param null|string $status        the status of the clearing request or null to retrieve all
     *
     * @return array<string, mixed> The clearing data
     *
     * @throws ClientException          if there is an error communicating with the server
     * @throws InvalidArgumentException if the cache implementation encounters an error
     * @throws ResponseException        Is thrown when the request did not succeed
     */
    public function entitledParty(string $entitledParty, ?string $status = null): array
    {
        $this->setApiKey();

        $url = '/clearing/entitled-party?entitled_party=' . $entitledParty;
        if (!is_null($status)) {
            $url .= '&status=' . $status;
        }

        $response = $this->requestService->get($url);

        if ($response->hasStatus(401)) {
            $message = 'Unauthorized for clearing request.';

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        if ($response->hasStatus(404)) {
            // NOT FOUND
            $this->logger->error('Entitle party not found: entitled-party: ' . $entitledParty);

            throw new ResponseException($response);
        }

        if (!$response->hasStatus(200)) {
            $this->logger->error('Something went wrong while trying to retrieve the clearing status from entitle party. entitled-party: ' . $entitledParty);

            throw new ResponseException($response);
        }

        return $response->json(true);
    }
}
