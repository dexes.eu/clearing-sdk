<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk\Repositories;

use Dexes\ClearingSdk\Exceptions\H2MApiKeyNotFoundInCacheException;
use Dexes\ClearingSdk\HttpRequestService;
use Dexes\ClearingSdk\Repositories\Auth\AuthRepository;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use XpertSelect\PsrTools\Exception\ClientException;

class BaseRepository
{
    /**
     * BaseRepository Constructor.
     *
     * @param HttpRequestService $requestService an instance of HttpRequestService
     * @param AuthRepository     $authRepository an instance of AuthRepository
     * @param LoggerInterface    $logger         an instance of LoggerInterface
     */
    public function __construct(
        protected HttpRequestService $requestService,
        protected AuthRepository $authRepository,
        protected LoggerInterface $logger,
        protected CacheInterface $cache,
        protected string $h2mCacheKey = '',
    ) {
    }

    /**
     * Sets the API key for the request Service.
     *
     * In case a h2mCacheKey is set, we will start the
     * H2M flow to get a token, otherwise we will operate in M2M mode.
     */
    public function setApiKey(): void
    {
        if (!empty($this->h2mCacheKey)) {
            $this->seth2mApiKey();
        } else {
            $this->setm2mApiKey();
        }
    }

    /**
     * Sets the H2M API key for the request service.
     *
     * This method retrieves the H2M API key from the cache, using some cachekey.
     * If the API key is not found in the cache, it throws an exception.
     *
     * The caller of this function can catch this exception and deal with it by generating a token
     * using the humanToMachineToken() function from the AuthRepository.
     *
     * @throws H2MApiKeyNotFoundInCacheException is thrown when the H2M API key was not found in the cache
     */
    public function seth2mApiKey(): void
    {
        if ($this->cache->has($this->h2mCacheKey)) {
            $apiKey = $this->cache->get($this->h2mCacheKey);

            $this->requestService->setApiKey($apiKey);
        } else {
            throw new H2MApiKeyNotFoundInCacheException();
        }
    }

    /**
     * Sets the API key for the request service.
     *
     * This method retrieves the API key from the cache using the client ID available in the auth repository.
     * If the API key is not found in the cache, it retrieves the machine token from the auth repository and stores it in the cache for 30 seconds.
     * The API key is then set in the request service by prepending it with 'Bearer '.
     *
     * @throws InvalidArgumentException is thrown when the cacheKey is not a legal value
     * @throws ClientException          Is thrown when an error occurs while communicating with the authRepo
     */
    protected function setm2mApiKey(): void
    {
        $cacheKey = $this->authRepository->getClientId();
        if ($this->cache->has($cacheKey)) {
            $apiKey = $this->cache->get($cacheKey);
        } else {
            $apiKey = $this->authRepository->machineToken();
            $this->cache->set($cacheKey, $apiKey, 30);
        }

        $this->requestService->setApiKey('Bearer ' . $apiKey);
    }
}
