<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk\Repositories\PolicyStore\Dataspace;

use Dexes\ClearingSdk\Repositories\BaseRepository;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class PolicyTemplateRepository.
 *
 * This class represents a repository for managing policy templates.
 */
class PolicyTemplateRepository extends BaseRepository
{
    /**
     * Retrieve all policy templates.
     *
     * @return array<string, mixed> the policy templates data in JSON format
     *
     * @throws ClientException   thrown when the request could not be sent
     * @throws ResponseException Is thrown when the request did not succeed
     */
    public function index(): array
    {
        $this->setApiKey();

        $response = $this->requestService->get('dataspace/policy-template');

        if (!$response->hasStatus(200)) {
            $this->logger->error('Failed to retrieve policy templates');

            throw new ResponseException($response);
        }

        return $response->json(true);
    }

    /**
     * Retrieve a policy template by ID.
     *
     * @param string $id the ID of the policy template to retrieve
     *
     * @return array<string, mixed> the policy template data in JSON format
     *
     * @throws ClientException   thrown when the request could not be sent
     * @throws ResponseException Is thrown when the request did not succeed
     */
    public function get(string $id): array
    {
        $this->setApiKey();

        $response = $this->requestService->get('dataspace/policy-template/' . $id);

        if (!$response->hasStatus(200)) {
            $this->logger->error('Failed to retrieve policy template' . $id);

            throw new ResponseException($response);
        }

        return $response->json(true);
    }
}
