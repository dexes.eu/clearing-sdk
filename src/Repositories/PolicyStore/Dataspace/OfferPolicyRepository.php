<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk\Repositories\PolicyStore\Dataspace;

use Dexes\ClearingSdk\Repositories\BaseRepository;
use Psr\SimpleCache\InvalidArgumentException;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\Exception\ResponseException;

/**
 * Class OfferPolicyRepository.
 *
 * The OfferPolicyRepository class provides methods for creating, retrieving, and deleting Offer Policies.
 */
class OfferPolicyRepository extends BaseRepository
{
    /**
     * Creates a new Offer Policy.
     *
     * @param string               $target          the target of the Offer Policy
     * @param string               $entitledParty   the entitlement party associated with the Offer Policy
     * @param string               $serviceProvider the service provider associated with the Offer Policy
     * @param array<string, mixed> $label           an array of labels to assign to the Offer Policy
     * @param array<int, array{
     *     policy_template_id: string,
     *     parameters?: string[],
     * }>        $policyTemplates an array of policy templates to associate with the Offer Policy
     *
     * @return array<string, mixed> returns the response from the API as an array
     *
     * @throws ClientException   thrown when the request could not be sent
     * @throws ResponseException Is thrown when the request did not succeed
     */
    public function post(array $policyTemplates, string $target, string $entitledParty, string $serviceProvider, array $label): array
    {
        $this->setApiKey();

        $body = [
            'policy_templates'    => $policyTemplates,
            'target'              => $target,
            'service_provider'    => $serviceProvider,
            'entitled_party'      => $entitledParty,
            'label'               => $label,
        ];

        $response = $this->requestService->post('dataspace/offer-policy', $body);

        if ($response->hasStatus(401)) {
            $message = 'Unauthorized to create new offer policy.';

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        if ($response->hasStatus(404)) {
            $message = 'policy template not found.';

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        if ($response->hasStatus(422)) {
            $message = 'Unprocessable entity ' . $response->getPsrResponse()->getBody()->getContents();

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        return $response->json(true);
    }

    /**
     * Retrieves all Offer Policies.
     *
     * @param string $filter (optional) The filter to apply when retrieving Offer Policies
     *
     * @return array<string, mixed> returns an array of Offer Policies
     *
     * @throws ClientException   thrown when the request could not be sent
     * @throws ResponseException Is thrown when the request did not succeed
     */
    public function index(string $filter = ''): array
    {
        $this->setApiKey();

        $filters = '' === $filter ? [] : ['target' => $filter];

        $response = $this->requestService->get('dataspace/offer-policy', parameters: $filters);

        if (!$response->hasStatus(200)) {
            $message = 'Failed to retrieve offer policies from the Policy Store.';

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        return $response->json(true);
    }

    /**
     * Retrieves an Offer Policy based on its index.
     *
     * @return array<string, mixed> returns the data of an Offer Policies
     *
     * @throws ClientException
     * @throws ResponseException
     * @throws InvalidArgumentException
     */
    public function get(string $identifier): array
    {
        $this->setApiKey();

        $response = $this->requestService->get('dataspace/offer-policy/' . $identifier);

        if ($response->hasStatus(404)) {
            $message = sprintf('No offer policy with id %s found. ', $identifier);

            $this->logger->error($message);

            throw new ResponseException($response);
        }

        if (!$response->hasStatus(200)) {
            $this->logger->error(sprintf('Something went wrong while trying to retrieve offer policy with id %s', $identifier));

            throw new ResponseException($response);
        }

        return $response->json(true);
    }

    /**
     * Deletes an Offer Policy by ID.
     *
     * @param string $id the ID of the Offer Policy to delete
     *
     * @return bool returns true if the Offer Policy was deleted successfully, false otherwise
     *
     * @throws ClientException thrown when the request could not be send
     */
    public function delete(string $id): bool
    {
        $this->setApiKey();

        $response = $this->requestService->delete('dataspace/offer-policy/' . $id);

        if ($response->hasStatus(401)) {
            $this->logger->error('Unauthorized to Delete offer policy ' . $id);

            return false;
        }

        return !($response->hasStatus(404));
    }
}
