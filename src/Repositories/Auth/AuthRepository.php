<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk\Repositories\Auth;

use Dexes\ClearingSdk\HttpRequestService;
use Dexes\ClearingSdk\IShare;
use Firebase\JWT\JWT;
use Psr\Log\LoggerInterface;
use XpertSelect\PsrTools\Exception\ClientException;
use XpertSelect\PsrTools\PsrResponse;

class AuthRepository
{
    /**
     * Constructs a new instance of the class.
     *
     * @param HttpRequestService $requestService the HTTP request service used for making API requests
     * @param LoggerInterface    $logger         the logger used for logging errors and messages
     * @param array<string>      $x5c            the array containing x5c values
     * @param string             $audience       the audience parameter value
     * @param string             $clientId       the client ID parameter value
     * @param string             $privateKey     the private key parameter value
     */
    public function __construct(
        private HttpRequestService $requestService,
        private LoggerInterface $logger,
        private readonly array $x5c,
        private readonly string $audience,
        private readonly string $clientId,
        private readonly string $privateKey,
    ) {
    }

    /**
     * Retrieves the client id.
     *
     * @return string the client id
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * Executes an POST request to the /connect/machine/token endpoint to get a M2M authentication token.
     *
     * @return string the H2M machine token
     *
     * @throws ClientException is thrown when an exception occurs while communicating with the Clearing component
     */
    public function machineToken(): string
    {
        $options = [
            'grant_type'            => IShare::GRANT_TYPE,
            'scope'                 => IShare::SCOPE,
            'client_id'             => $this->clientId,
            'client_assertion_type' => IShare::CLIENT_ASSERTION_TYPE,
            'client_assertion'      => $this->getClientAssertion(),
        ];

        $result = $this->requestService->postEncoded('connect/machine/token', $options);

        if (!$result->hasStatus(200)) {
            $message = 'Failed to retrieve the M2M token';
            $this->logger->error($message);

            throw new ClientException($message);
        }

        return $result->json(true)['access_token'];
    }

    /**
     * Start Human to Machine (H2M) token flow.
     *
     * Note that the result of this function should be manually turned into a
     * redirect response by the caller of this function. This redirect response
     * should redirect to the 'location' header of the result.
     *
     * @throws ClientException when status of response is not 303
     */
    public function humanToMachineToken(string $redirectUri): PsrResponse
    {
        $options = [
            'redirect_uri' => $redirectUri,
        ];

        $result = $this->requestService->get('connect/human/auth', $options);

        if (!$result->hasStatus(303)) {
            $message = 'Did not receive status 303 from connect/human/auth endpoint';
            $this->logger->error($message);

            throw new ClientException($message);
        }

        return $result;
    }

    /**
     * Generates the client assertion.
     *
     * This method generates a client assertion in JSON Web Token (JWT) format.
     *
     * @return string the client assertion string
     */
    private function getClientAssertion(): string
    {
        $header = [
            'typ' => 'JWT',
            'x5c' => $this->x5c,
        ];

        $payload = [
            'iss' => $this->clientId,
            'sub' => $this->clientId,
            'nbf' => time(),
            'aud' => $this->audience,
            'jti' => uniqid(),
            'iat' => time(),
            'exp' => time() + 30,
        ];

        // @phpstan-ignore-next-line
        return JWT::encode($payload, $this->privateKey, IShare::JWT_ALGORITHM, null, $header);
    }
}
