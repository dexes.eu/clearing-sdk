<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Dexes\ClearingSdk;

/**
 * Class IShare.
 *
 * Holding various IShare constants
 */
class IShare
{
    public const SCOPE = 'iSHARE';

    public const GRANT_TYPE = 'client_credentials';

    public const CLIENT_ASSERTION_TYPE = 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer';

    public const TYP = 'JWT';

    public const JWT_ALGORITHM = 'RS256';
}
