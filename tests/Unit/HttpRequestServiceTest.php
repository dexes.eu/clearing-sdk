<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Dexes\ClearingSdk\HttpRequestService;
use GuzzleHttp\Psr7\HttpFactory;
use GuzzleHttp\Psr7\Request;
use Mockery as M;
use PHPUnit\Framework\Assert;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;

/**
 * @internal
 */
class HttpRequestServiceTest extends TestCase
{
    public static function endpointDataset(): array
    {
        return [
            ['https://example.com', 'https://example.com'],
            ['https://example.com/', 'https://example.com'],
            ['https://example.com:8080', 'https://example.com:8080'],
            ['https://example.com:8080/', 'https://example.com:8080'],
            ['https://example.com/foo', 'https://example.com/foo'],
            ['https://example.com/foo/', 'https://example.com/foo'],
            ['https://example.com:8080/foo', 'https://example.com:8080/foo'],
            ['https://example.com:8080/foo/', 'https://example.com:8080/foo'],
        ];
    }

    public function testHasApiKey(): void
    {
        $httpRequestService = new HttpRequestService(
            '',
            M::mock(ClientInterface::class),
            M::mock(RequestFactoryInterface::class),
            M::mock(StreamFactoryInterface::class)
        );

        Assert::assertFalse($httpRequestService->hasApiKey());

        $httpRequestService->setApiKey('foo');

        Assert::assertTrue($httpRequestService->hasApiKey());
    }

    /**
     * @dataProvider endpointDataset
     */
    public function testTrailingSlashIsRemovedFromEndpoint(string $endpoint, string $validated): void
    {
        $httpRequestService = new HttpRequestService(
            $endpoint,
            M::mock(ClientInterface::class),
            M::mock(RequestFactoryInterface::class),
            M::mock(StreamFactoryInterface::class)
        );

        Assert::assertEquals($validated, $httpRequestService->getEndpoint());
    }

    public function testGetCreatesPsrGetRequest(): void
    {
        try {
            $httpRequestService = new HttpRequestService(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            Assert::assertEmpty($this->guzzleHistory);

            $httpRequestService->get('bar', ['lorem' => 'ipsum']);

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals('GET', strtoupper($request->getMethod()));
                Assert::assertEquals('/bar', $request->getUri()->getPath());
                Assert::assertEquals('lorem=ipsum', $request->getUri()->getQuery());

                Assert::assertEquals(['application/json'], $request->getHeader('Accept'));
                Assert::assertEquals(['dexes/clearing-sdk'], $request->getHeader('User-Agent'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }

    public function testApiKeyIsIncludedAsAuthorizationHeader(): void
    {
        try {
            $httpRequestService = new HttpRequestService(
                'https://example.com',
                $this->createPsrClient(),
                new HttpFactory(),
                new HttpFactory()
            );

            Assert::assertEmpty($this->guzzleHistory);
            Assert::assertFalse($httpRequestService->hasApiKey());

            $httpRequestService->setApiKey('foo');
            $httpRequestService->get('/');

            Assert::assertNotEmpty($this->guzzleHistory);

            foreach ($this->guzzleHistory as $entry) {
                Assert::assertArrayHasKey('request', $entry);

                /** @var Request $request */
                $request = $entry['request'];

                Assert::assertEquals(['foo'], $request->getHeader('Authorization'));
            }
        } catch (ClientException $e) {
            $this->fail($e->getMessage());
        }
    }
}
