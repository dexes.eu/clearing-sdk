<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Dexes\ClearingSdk\ClearingResponse;
use Dexes\ClearingSdk\HttpRequestService;
use Dexes\ClearingSdk\Repositories\Auth\AuthRepository;
use Psr\Log\LoggerInterface;
use Tests\TestCase;
use XpertSelect\PsrTools\Exception\ClientException;

/**
 * @internal
 */
class HumanToMachineTokenTest extends TestCase
{
    /**
     * Test whether the function humanToMachineToken() calls the HttpRequestService
     * with correct arguments when a non-encoded redirect URI is provided.
     */
    public function testNonEncodedUri(): void
    {
        $nonEncodedRedirectUri = 'http://example.com/search?q=test&fl=name';

        $httpRequestServiceMock = $this->createMock(HttpRequestService::class);
        $responseMock           = $this->createMock(ClearingResponse::class);
        $responseMock->method('hasStatus')->with(303)->willReturn(true);

        $logger = $this->createMock(LoggerInterface::class);

        $httpRequestServiceMock->expects($this->once())
            ->method('get')
            ->with(
                'connect/human/auth',
                ['redirect_uri' => $nonEncodedRedirectUri]
            )->willReturn($responseMock);

        $authRepository = new AuthRepository($httpRequestServiceMock, $logger, [], '', '', '');

        $authRepository->humanToMachineToken($nonEncodedRedirectUri);
    }

    /**
     * Test whether the function humanToMachineToken() calls the HttpRequestService
     * with correct arguments when an already encoded redirect URI is provided.
     */
    public function testAlreadyEncodedUri(): void
    {
        $encodedRedirectUri = 'http%3A%2F%2Fexample.com%2Fsearch%3Fq%3Dtest%26fl%3Dname';

        $httpRequestServiceMock = $this->createMock(HttpRequestService::class);
        $responseMock           = $this->createMock(ClearingResponse::class);
        $responseMock->method('hasStatus')->with(303)->willReturn(true);

        $logger = $this->createMock(LoggerInterface::class);

        $httpRequestServiceMock->expects($this->once())
            ->method('get')
            ->with(
                'connect/human/auth',
                ['redirect_uri' => $encodedRedirectUri]
            )->willReturn($responseMock);

        $authRepository = new AuthRepository($httpRequestServiceMock, $logger, [], '', '', '');

        $authRepository->humanToMachineToken($encodedRedirectUri);
    }

    /**
     * Test whether the function humanToMachineToken() throws a ClientException error
     * when the HttpRequestService did not have status 303.
     */
    public function testErrorThrownOnNon303(): void
    {
        $this->expectException(ClientException::class);
        $redirectUri = 'https://example.com/query?q=test&name=test';

        $httpRequestServiceMock = $this->createMock(HttpRequestService::class);
        $responseMock           = $this->createMock(ClearingResponse::class);
        $responseMock->method('hasStatus')->with(303)->willReturn(false);

        $logger = $this->createMock(LoggerInterface::class);

        $httpRequestServiceMock->expects($this->once())
            ->method('get')
            ->with(
                'connect/human/auth',
                ['redirect_uri' => $redirectUri]
            )->willReturn($responseMock);

        $authRepository = new AuthRepository($httpRequestServiceMock, $logger, [], '', '', '');

        $authRepository->humanToMachineToken($redirectUri);
    }
}
