<?php

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests\Unit;

use Dexes\ClearingSdk\ClearingResponse;
use Mockery;
use PHPUnit\Framework\Assert;
use Psr\Http\Message\ResponseInterface;
use Tests\TestCase;

/**
 * @internal
 */
class ClearingResponseTest extends TestCase
{
    public function testOriginalResponse(): void
    {
        $psrResponse             = Mockery::mock(ResponseInterface::class);
        $clearingResponse        = new ClearingResponse($psrResponse);

        Assert::assertSame($clearingResponse->getPsrResponse(), $psrResponse);
    }
}
