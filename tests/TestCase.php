<?php

declare(strict_types=1);

/**
 * This file is part of the dexes/clearing-sdk package.
 *
 * This source file is subject to the license that is
 * bundled with this source code in the LICENSE.md file.
 */

namespace Tests;

use Dexes\ClearingSdk\ClearingResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use Mockery as M;
use Mockery\MockInterface as MI;
use PHPUnit\Framework\TestCase as BaseTestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * @internal
 */
abstract class TestCase extends BaseTestCase
{
    protected array $guzzleHistory;

    public function setUp(): void
    {
        parent::setUp();

        $this->guzzleHistory = [];
    }

    protected function loadFixture(string $fixture): string
    {
        return file_get_contents(__DIR__ . '/fixtures/' . $fixture);
    }

    protected function createMockedResponse(int $status, string $fixture): ClearingResponse
    {
        $psrResponse = M::mock(ResponseInterface::class, function(MI $mock) use ($status, $fixture) {
            $stream = M::mock(StreamInterface::class, function(MI $mock) use ($fixture) {
                $mock->shouldReceive('getContents')
                    ->andReturn($this->loadFixture($fixture));
            });

            $mock->shouldReceive('getStatusCode')->andReturn($status);
            $mock->shouldReceive('getBody')->andReturn($stream);
        });

        return new ClearingResponse($psrResponse);
    }

    protected function createPsrClient(): ClientInterface
    {
        $history = Middleware::history($this->guzzleHistory);
        $mock    = new MockHandler([new Response()]);

        $handlerStack = HandlerStack::create($mock);
        $handlerStack->push($history);

        return new Client(['handler' => $handlerStack]);
    }
}
